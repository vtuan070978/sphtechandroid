package com.example.sphtech.api

import com.example.sphtech.ui.showdata.viewmodel.ShowDataService

object ApiFactory {
    val showDataService: ShowDataService = RetrofitFactory.retrofit().create(ShowDataService::class.java)

}