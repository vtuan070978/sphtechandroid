package com.example.sphtech.ui.showdata.viewmodel

import com.example.sphtech.ui.showdata.model.DataListModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ShowDataService {

    @GET("/api/action/datastore_search")
    fun getDataList(@Query("offset") offset: Int, @Query("limit") limit: Int, @Query("resource_id") resource_id: String): Single<DataListModel>

    @GET
    fun getDataListNextOrPre(@Url url: String): Single<DataListModel>
}
