package com.example.sphtech.ui.showdata.view

import com.example.sphtech.ui.showdata.model.ResultModel

sealed class ShowDataViewState {
    data class Loading(val isLoading: Boolean) : ShowDataViewState()
    data class Error(val message: String?) : ShowDataViewState()
    data class LoadDataSource(val data: ResultModel?) : ShowDataViewState()
}