package com.example.sphtech.ui.showdata.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LinkModel(
    val start: String? = null,
    val prev: String? = null,
    val next: String? = null
): Parcelable