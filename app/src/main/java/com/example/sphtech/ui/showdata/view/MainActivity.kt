package com.example.sphtech.ui.showdata.view

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sphtech.R
import com.example.sphtech.ui.showdata.model.RecordModel
import com.example.sphtech.ui.showdata.model.ResultModel
import com.example.sphtech.ui.showdata.viewmodel.SqlliteHandler
import com.tbruyelle.rxpermissions2.RxPermissions
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onStop
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_header_layout.*
import timber.log.Timber
import xyz.medigo.user.ui.historyconsultationform.ShowDataAdapter

@Suppress("NAME_SHADOWING")
class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ShowDataViewModel
    private lateinit var mAdapter: ShowDataAdapter
    private var mOffset: Int = 0
    private var mLimit: Int = 5
    private var mTotal: Int = 0
    private var resourceId: String = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"
    private var iQuarterSort: Int = 0
    private var iDataSort: Int = 0
    var sqlliteHandler: SqlliteHandler = SqlliteHandler(
        this,
        SqlliteHandler.DATABASE_NAME,
        null,
        SqlliteHandler.DATABASE_VERSION
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAdapter = ShowDataAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mAdapter

        viewModel = ViewModelProviders.of(this).get(ShowDataViewModel::class.java)
        observe(viewModel.state, this::onViewStateChanged)

        onPulltoRefresh()
    }

    override fun onResume() {
        super.onResume()
        checkandShowPermission()
    }

    private fun onPulltoRefresh() {
        swipeContainer.setOnRefreshListener { // Your code to refresh the list here.
            // Make sure you call swipeContainer.setRefreshing(false)
            // once the network request has completed successfully.
            swipeContainer.isRefreshing = false
            viewModel.callApiGetdata(mOffset, mLimit, resourceId)
        }
    }
    private fun onViewStateChanged(viewState: ShowDataViewState) {
        when (viewState) {
            is ShowDataViewState.Loading -> {
                if (viewState.isLoading) {
                    //show loading
                    progressBar.visibility = View.VISIBLE
                } else {
                    // hide loading
                    progressBar.visibility = View.GONE
                }
            }
            is ShowDataViewState.LoadDataSource -> {
                swipeContainer.isRefreshing = false
                viewState.data?.let { it ->
                    it.records?.let {
                        if (it.size > 0) {
                            mLimit = viewState.data.limit!!
                            mTotal = viewState.data.total!!
                            if (null != viewState.data.offset) {
                                mOffset = viewState.data.offset
                            } else
                                mOffset = 0
                            resetSetting()
                            mAdapter.result = viewState.data
                            mAdapter.notifyDataSetChanged()
                            sqlliteHandler.saveResultData(viewState.data)
                        }
                    }
                }
            }
            is ShowDataViewState.Error -> {
                //show error
                swipeContainer.isRefreshing = false
                Timber.d( "get data failed: ${viewState.message}")
                if (viewState.message.equals("lost_internet")) {

                    var recordModel = sqlliteHandler.getRecord(resourceId, mOffset, mLimit)
                    recordModel?.let {
                        var resultModel = ResultModel(resourceId, null, recordModel, null, mOffset, mLimit)
                        if (resultModel?.records != null) {
                            resetSetting()
                            mAdapter.result = resultModel
                            mAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        }
    }

    private fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btnPre -> {
                mAdapter.result?.let {
                    it.links?.let { it ->
                        it.prev?.let {
                            viewModel.onNextOrPre(it)
                            return
                        }
                        it.start?. let {
                            viewModel.onNextOrPre(it)
                        }
                    }
                    if (it.links == null) {
                        mOffset-= mLimit;
                        if (mOffset <0 )
                            mOffset = 0
                        viewModel.callApiGetdata(mOffset, mLimit, resourceId)
                    }
                }
            }
            R.id.btnNext -> {
                mAdapter.result?.let {
                    it.links?.let { it ->
                        it.next?.let {
                            viewModel.onNextOrPre(it)
                        }
                    }
                    if (it.links == null) {
                        mOffset+= mLimit;
                        viewModel.callApiGetdata(mOffset, mLimit, resourceId)
                    }
                }
            }
            R.id.imgQuarterSort -> {
                iQuarterSort++
                if (iQuarterSort == 3)
                    iQuarterSort = 1
                var lst = mAdapter.result?.records

                updateSortImage(imgQuarterSort, iQuarterSort)
                lst = lst?.let { dataSort(it, 0, iQuarterSort) }
                lst?.forEach { println(it) }
                mAdapter.result?.records = lst
                mAdapter.notifyDataSetChanged()
                iDataSort = 0
                updateSortImage(imgDataSort, iDataSort)
            }
            R.id.imgDataSort -> {
                iDataSort++
                if (iDataSort == 3)
                    iDataSort = 1
                var lst = mAdapter.result?.records

                updateSortImage(imgDataSort, iDataSort)
                mLimit
                lst = lst?.let { dataSort(it, 1, iDataSort) }
                lst?.forEach { println(it) }
                mAdapter.result?.records = lst
                mAdapter.notifyDataSetChanged()
                iQuarterSort = 0
                updateSortImage(imgQuarterSort, iQuarterSort)
            }
        }
    }

    private fun resetSetting() {
        iQuarterSort = 0
        iDataSort = 0
        updateSortImage(imgQuarterSort, iDataSort)
        updateSortImage(imgDataSort, iQuarterSort)
    }

    private fun updateSortImage(img: ImageView, sortType: Int) {
        when (sortType) {
            0 -> {
                img.setImageDrawable(getDrawable(R.mipmap.updown))
            }
            1 -> {
                img.setImageDrawable(getDrawable(R.mipmap.arrow_up))
            }
            2 -> {
                img.setImageDrawable(getDrawable(R.mipmap.arrow_down))
            }
        }
    }

    private fun dataSort(lst: List<RecordModel>, typeData: Int, sortType: Int): List<RecordModel> {
        val lstTemp: List<RecordModel>
        lstTemp = if (sortType == 1) {
            if (typeData ==0 )
                lst.sortedBy { it.quarter }
            else
                lst.sortedBy { it.volume_of_mobile_data.toFloat() }
        } else {
            if (typeData ==0 ) {
                lst.sortedByDescending { it.quarter }
            } else
                lst.sortedByDescending { it.volume_of_mobile_data.toFloat() }
        }
        return lstTemp
    }

    fun checkandShowPermission() {
        RxPermissions(this)
            .requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe { permission ->
                when {
                    permission.granted -> {
                        viewModel.callApiGetdata(mOffset, mLimit, resourceId)
                    }
                    permission.shouldShowRequestPermissionRationale -> {
                        // Denied permission without ask never again
                    }
                    else -> {
                        // Denied permission with ask never again
                        // Need to go to the settings
                    }
                }
            }
            .disposeBy(onStop)
    }
}


