package com.example.sphtech.ui.showdata.viewmodel

import android.content.Context
import com.example.sphtech.extenstions.NetworkException
import com.example.sphtech.extenstions.mapNetworkErrors
import com.example.sphtech.ui.showdata.model.DataListModel
import io.reactivex.Single
import java.io.IOException
import java.lang.Error

class ShowDataRepositoryImp(val apiService: ShowDataService) : ShowDataRepository{

    override fun getDataList(
        offset: Int,
        limit: Int,
        resourceId: String
    ): Single<DataListModel> {
        return apiService.getDataList(offset, limit, resourceId)
            .mapNetworkErrors()
            .onErrorResumeNext { error ->
                if (error is NetworkException || error is IOException) {
                    Single.just(DataListModel(help = "lost_internet", success = false, result = null))

                } else {
                    Single.error(error)
                }
            }
    }

    override fun getDataListNextOrPre(url: String): Single<DataListModel> {
        return apiService.getDataListNextOrPre(url)
            .mapNetworkErrors()
            .onErrorResumeNext { error ->
                if (error is NetworkException || error is IOException) {
                    Single.just(DataListModel(help = "lost_internet", success = false, result = null))

                } else {
                    Single.error(error)
                }
            }
    }
}