package com.example.sphtech.ui.showdata.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.sphtech.api.ApiFactory
import com.example.sphtech.base.BaseViewModel
import com.example.sphtech.extenstions.NetworkException
import com.example.sphtech.ui.showdata.viewmodel.ShowDataRepository
import com.example.sphtech.ui.showdata.viewmodel.ShowDataRepositoryImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ShowDataViewModel : BaseViewModel() {

    private val _state = MutableLiveData<ShowDataViewState>()
    val state: LiveData<ShowDataViewState>
        get() = _state

    private val showDataRepository: ShowDataRepository =
        ShowDataRepositoryImp(ApiFactory.showDataService)

    private var disposable: Disposable? = null

    fun callApiGetdata(ofsset: Int, limit: Int, resourceId: String) {
        _state.value = ShowDataViewState.Loading(true)
        disposable =
            showDataRepository.getDataList(ofsset, limit, resourceId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ ->  _state.value = ShowDataViewState.Loading(false) }
                .subscribe({ result ->
                    if (result.success == true)
                        _state.postValue(ShowDataViewState.LoadDataSource(result.result))
                    else
                        _state.value = ShowDataViewState.Error(result.help)
                }, { throwable ->
                    _state.value = ShowDataViewState.Error(throwable.localizedMessage)
                }
                )
    }

    fun onNextOrPre(urlNext: String) {
        _state.value = ShowDataViewState.Loading(true)
        disposable =
            showDataRepository.getDataListNextOrPre(urlNext)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> _state.value = ShowDataViewState.Loading(false) }
                .subscribe({ result ->
                    if (result.success == true)
                        _state.postValue(ShowDataViewState.LoadDataSource(result.result))
                    else {
                        _state.value = ShowDataViewState.Error(result.help)
                    }
                }, { throwable ->
                    if (throwable is NetworkException)
                        _state.value = ShowDataViewState.Error(throwable.localizedMessage)
                    else
                        _state.value = ShowDataViewState.Error(throwable.localizedMessage)
                }
                )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.isDisposed
    }
}