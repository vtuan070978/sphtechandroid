package com.example.sphtech.ui.showdata

import android.view.View
import com.example.sphtech.base.BaseClass

interface ShowDataInterface : BaseClass.Presenter<View> {

    fun loadData()
    fun loadDataAll()

}