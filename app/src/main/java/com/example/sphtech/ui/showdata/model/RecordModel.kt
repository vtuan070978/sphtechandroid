package com.example.sphtech.ui.showdata.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecordModel (val volume_of_mobile_data: String, val quarter: String, val _id: String) :
     Parcelable