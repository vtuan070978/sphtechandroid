package com.example.sphtech.ui.showdata.viewmodel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.example.sphtech.ui.showdata.model.FieldModel;
import com.example.sphtech.ui.showdata.model.RecordModel;
import com.example.sphtech.ui.showdata.model.ResultModel;
import java.util.ArrayList;
import timber.log.Timber;


public class SqlliteHandler extends SQLiteOpenHelper {

    // Database Version
    public static int DATABASE_VERSION = 1;
    // Database Name
    public static String DATABASE_NAME = "showData";
    // User table name
    private String TABLE_FIELDS = "fields";
    private String TABLE_RECORDS = "records";
    private String TABLE_LINKS = "links";

    // User Table Columns names
    private static final String KEY_FIELD_RESOURCE_ID = "resourceid";
    private static final String KEY_FIELD_ID = "fieldid";
    private static final String KEY_FIELD_TYPE = "type";

    private static final String KEY_RECORD_RESOURCE_ID = "resourceid";
    private static final String KEY_RECORD_ID = "recordid";
    private static final String KEY_RECORD_QUARTER = "quarter";
    private static final String KEY_RECORD_VOLUME_DATA = "volumedata";

    private static final String KEY_LINK_RESOURCE_ID = "resourceid";
    private static final String KEY_LINK_RESOURCE_START = "start";
    private static final String KEY_LINK_RESOURCE_NEXT = "next";


    public SqlliteHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public SqlliteHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create table user
        String CREATE_TABLE_FIELDS = "CREATE  TABLE " + TABLE_FIELDS + "("
                + KEY_FIELD_RESOURCE_ID + " TEXT NOT NULL , "
                + KEY_FIELD_ID + " TEXT NOT NULL, "
                + KEY_FIELD_TYPE + " TEXT NOT NULL)";
        db.execSQL(CREATE_TABLE_FIELDS);

//        // Create table records
        String CREATE_TABLE_RECORDS = "CREATE  TABLE " + TABLE_RECORDS + "("
                + KEY_RECORD_RESOURCE_ID + " TEXT NOT NULL , "
                + KEY_RECORD_ID + " TEXT NOT NULL, "
                + KEY_RECORD_QUARTER + " TEXT NOT NULL, "
                + KEY_RECORD_VOLUME_DATA + " TEXT NOT NULL)";
        db.execSQL(CREATE_TABLE_RECORDS);

        // Create table links
        String CREATE_TABLE_LINKS = "CREATE  TABLE " + TABLE_LINKS + "("
                + KEY_LINK_RESOURCE_ID + " TEXT NOT NULL , "
                + KEY_LINK_RESOURCE_START + " TEXT NOT NULL , "
                + KEY_LINK_RESOURCE_NEXT +" TEXT NOT NULL)";
        db.execSQL(CREATE_TABLE_LINKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FIELDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINKS);
        // Create tables again
        onCreate(db);
    }

    /**
     * add user
     */

    //Insert values to the table Fields
    private void addFieldItem(String resourceid, String fieldid, String type){
        String _fieldid = "";
        SQLiteDatabase db = this.getWritableDatabase();
        try{

            String queryString = "Select " + KEY_FIELD_ID + " from " + TABLE_FIELDS +
                    " WHERE  " + KEY_FIELD_ID + " = " + fieldid ;
            Cursor c = db.rawQuery(queryString,null);

            if(c.moveToFirst()){
                _fieldid = c.getString(0);
            }
            c.close();

            ContentValues values=new ContentValues();
            values.put(KEY_FIELD_RESOURCE_ID, resourceid);
            values.put(KEY_FIELD_ID, fieldid);
            values.put(KEY_FIELD_TYPE, type);

            if (!TextUtils.isEmpty(_fieldid))
                db.update(TABLE_FIELDS, values, KEY_FIELD_ID + " = ?",
                        new String[] { resourceid });
            else
                db.insert(TABLE_FIELDS, null, values);
        }catch(Exception e){
            Timber.e(e);
        }
    }

    //Insert values to the table records
    public boolean addRecordItem(String resourceid, String recordid, String quarter, String  volume_of_mobile_data){
        String _recordid = "";
        SQLiteDatabase db = this.getWritableDatabase();
        try{

            String queryString = "Select " + KEY_RECORD_ID + " from " + TABLE_RECORDS +
                    " WHERE  " + KEY_RECORD_ID + " = " + recordid ;
            Cursor c = db.rawQuery(queryString,null);
            if(c.moveToFirst()){
                _recordid = c.getString(0);
            }
            c.close();

            ContentValues values=new ContentValues();
            values.put(KEY_RECORD_RESOURCE_ID, resourceid);
            values.put(KEY_RECORD_ID, recordid);
            values.put(KEY_RECORD_QUARTER, quarter);
            values.put(KEY_RECORD_VOLUME_DATA, volume_of_mobile_data);

            if (!TextUtils.isEmpty(_recordid))
                db.update(TABLE_RECORDS, values, KEY_RECORD_ID + " = ?",
                        new String[] { _recordid });
            else
                db.insert(TABLE_RECORDS, null, values);

        }catch(Exception e){
            Timber.e(e);
            return false;
        }
        return true;
    }

    /**
     * add transaction
     */

    //Insert values to the table Link
    public boolean addLinkItem(String resourceId, String start, String next){
        String linkID = "";
        SQLiteDatabase db = this.getWritableDatabase();
        try{

            String GET_LAST_USER_ID = "Select " + KEY_LINK_RESOURCE_ID + " from " + TABLE_LINKS;
            Cursor c = db.rawQuery(GET_LAST_USER_ID,null);
            if(c.moveToFirst()){
                linkID = c.getString(0);
            }
            c.close();

            ContentValues values=new ContentValues();
            values.put(KEY_LINK_RESOURCE_ID, resourceId);
            values.put(KEY_LINK_RESOURCE_START, start);
            values.put(KEY_LINK_RESOURCE_NEXT, next);
            if (!TextUtils.isEmpty(linkID))
                db.update(TABLE_LINKS, values, KEY_LINK_RESOURCE_ID + " = ?",
                        new String[] { linkID });
            else
                db.insert(TABLE_LINKS, null, values);

        }catch(Exception e){
            Timber.e(e);
            return false;
        }
        return true;
    }

    public ArrayList<FieldModel> getFields() {
        ArrayList<FieldModel> lst = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT *" + " FROM " + TABLE_FIELDS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                FieldModel fieldModel = new FieldModel(cursor.getString(1), cursor.getString(0));
                lst.add(fieldModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return field list
        return lst;
    }
    /*
        LIMIT offset, row_count
    */
    public ArrayList<RecordModel> getRecord(String resourceId, int offset, int limit) {
        ArrayList<RecordModel> lst = new ArrayList<>();
        String selectQuery = "SELECT * " + " FROM " + TABLE_RECORDS + " LIMIT " + limit + " offset " + offset;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RecordModel recordModel = new RecordModel(cursor.getString(3), cursor.getString(2), cursor.getString(1));
                lst.add(recordModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lst;
    }

    public void saveResultData(ResultModel resultModel) {
        try {
            // save type
            for (int i = 0; i< resultModel.getFields().size(); i++) {
                FieldModel fieldModel = resultModel.getFields().get(i);
                addFieldItem(resultModel.getResourceId(), fieldModel.getId(), fieldModel.getType());
            }
            // save record
            for (int i = 0; i< resultModel.getRecords().size(); i++) {
                RecordModel recordModel = resultModel.getRecords().get(i);
                addRecordItem(resultModel.getResourceId(), recordModel.get_id(), recordModel.getQuarter(),
                        recordModel.getVolume_of_mobile_data());
            }
        } catch (Exception ex) {
            Timber.e(ex);
        }
    }
}
