package com.example.sphtech.ui.showdata.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataListModel(
    val help : String?= null,
    val success : Boolean?= null,
    val result: ResultModel?= null
):Parcelable {
}