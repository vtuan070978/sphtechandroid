package com.example.sphtech.ui.showdata.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ResultModel(
    @SerializedName("resource_id")
    @Expose
    val resourceId: String?= null,
    @SerializedName("fields")
    @Expose
    val fields: List<FieldModel>?= null,
    @SerializedName("records")
    @Expose
    var records: List<RecordModel>?= null,
    @SerializedName("_links")
    @Expose
    val links: LinkModel?= null,
    @SerializedName("offset")
    @Expose
    val offset: Int?= null,
    @SerializedName("limit")
    @Expose
    val limit: Int?= null,
    @SerializedName("total")
    @Expose
    val total: Int?= null
): Parcelable