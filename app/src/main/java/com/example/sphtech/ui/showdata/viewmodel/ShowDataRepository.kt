package com.example.sphtech.ui.showdata.viewmodel

import com.example.sphtech.ui.showdata.model.DataListModel
import io.reactivex.Single
import retrofit2.http.Url

interface ShowDataRepository {
    fun getDataList(offset: Int, limit: Int, resource_id: String): Single<DataListModel>

    fun getDataListNextOrPre(@Url url: String): Single<DataListModel>
}