package xyz.medigo.user.ui.historyconsultationform

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sphtech.R
import com.example.sphtech.ui.showdata.model.RecordModel
import com.example.sphtech.ui.showdata.model.ResultModel
import kotlinx.android.synthetic.main.item_data_layout.view.*


class ShowDataAdapter (
    private val mContext: Context,
    var result: ResultModel? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun onCreateViewHolder(pParent: ViewGroup, pViewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.item_data_layout, pParent, false)
        return ItemViewHolder(itemView)
    }


    override fun getItemCount(): Int {
        return result?.records?.size ?: 0
    }

    override fun onBindViewHolder(pViewHolder: RecyclerView.ViewHolder, pPosition: Int) {
        when (pViewHolder) {
            is ItemViewHolder -> pViewHolder.bind(pPosition)
        }
    }

    inner class ItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(pPosition: Int) {
            val record: RecordModel? = result?.records?.get(pPosition)
            itemView.tvQuarter.text = record?.quarter?: ""
            itemView.tvVolumeData.text = record?.volume_of_mobile_data?: ""
        }
    }

}

