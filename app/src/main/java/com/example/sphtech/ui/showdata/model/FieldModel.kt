package com.example.sphtech.ui.showdata.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FieldModel(val type: String, val id: String) : Parcelable