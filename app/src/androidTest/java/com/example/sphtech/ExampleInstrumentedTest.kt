package com.example.sphtech

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.sphtech.ui.showdata.view.MainActivity
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    var activityScenarioRule =
        ActivityScenarioRule(
            MainActivity::class.java
        )
    @Before
    fun setup() {

    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.sphtech", appContext.packageName)
    }

    @Test
    fun testClickOnNextButton() {
        Thread.sleep(2000)
        onView(withId(R.id.btnNext)).perform(click())
        Thread.sleep(2000)
        onView(withId(R.id.imgDataSort)).perform(click())
        Thread.sleep(2000)
    }

//    @Test
//    fun testClickSortImage() {
//        Thread.sleep(2000)
//        onView(withId(R.id.imgSort)).perform(click())
//        Thread.sleep(2000)
//    }

}
